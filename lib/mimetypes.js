
var _ = require('lodash')
  , libphonenumber = require('libphonenumber')
  , configLoader = require('../configLoader')


// commonMime
var commonMime = {};
commonMime.data = "data1";
commonMime.type = "data2";
commonMime.label = "data3";


// StructuredName
// "displayName": "",
// "firstName": "",
// "middleName": "",
// "lastName": "",
// "suffix": "",
// "salutation": ""

var nameMime = {};
nameMime.content_item_type = "vnd.android.cursor.item/name";
nameMime.display_name = "data1";
nameMime.given_name = "data2";
nameMime.family_name = "data3";
nameMime.prefix = "data4";
nameMime.middle_name = "data5";
nameMime.suffix = "data6";
nameMime.phonetic_given_name = "data7";
nameMime.phonetic_middle_name = "data8";
nameMime.phonetic_family_name = "data9";

nameMime.schema = {
  "nick": "",
  "displayName": "",
  "firstName": "",
  "middleName": "",
  "lastName": "",
  "suffix": "",
  "salutation": ""
};

nameMime.mapping = {
  display_name: "displayName",
  given_name: "firstName",
  middle_name: "middleName",
  family_name: "lastName",
  suffix: "suffix",
  prefix: "salutation"
};

nameMime.destField = 'name';
nameMime.destFieldIsArray = false;



// Nickname
var nickMime = {};
nickMime.content_item_type = "vnd.android.cursor.item/nickname";
nickMime.name = "data1";
nickMime.type_default = 1;
nickMime.type_other_name = 2;
nickMime.type_maiden_name = 3;
nickMime.type_short_name = 4;
nickMime.type_initials = 5;

nickMime.schema = {
  "nick": ""
};

nickMime.mapping = {
  name: "nick"
};

nickMime.destField = 'name';
nickMime.destFieldIsArray = false;




var phoneMime = {};
phoneMime.content_item_type = "vnd.android.cursor.item/phone_v2";
// phoneMime.content_type = "vnd.android.cursor.dir/phone_v2";
phoneMime.number = "data1";
phoneMime.type = commonMime.type;
phoneMime.label = commonMime.label;
phoneMime.normalized_number = "data4";
phoneMime.type_home = 1;
phoneMime.type_mobile = 2;
phoneMime.type_work = 3;
phoneMime.type_fax_work = 4;
phoneMime.type_fax_home = 5;
phoneMime.type_pager = 6;
phoneMime.type_other = 7;
phoneMime.type_callback = 8;
phoneMime.type_car = 9;
phoneMime.type_company_main = 10;
phoneMime.type_isdn = 11;
phoneMime.type_main = 12;
phoneMime.type_other_fax = 13;
phoneMime.type_radio = 14;
phoneMime.type_telex = 15;
phoneMime.type_tty_tdd = 16;
phoneMime.type_work_mobile = 17;
phoneMime.type_work_pager = 18;
phoneMime.type_assistant = 19;
phoneMime.type_mms = 20;

phoneMime.schema = {
  "type": "",
  "number": "",
  "label": ""
};

phoneMime.mapping = {
  number: "number",
  type: "type",
  label: "label"
};

phoneMime.destField = 'phones';
phoneMime.destFieldIsArray = true;

phoneMime.dupTest = {
  srcField: phoneMime.number,
  destField: phoneMime.mapping.number
};


phoneMime.parse = function(src, dest, callback) {

  var self = this;

  // Can't handle all possible special cases, but...here's one
  var extractMult = src.data1.match(/([\+0-9]+[\- ]*[0-9]+)\s*\/\s*([\+0-9]+[\- ]*[0-9]+)/);  //+91 9724176906 / +91-9998823966

  if(!!extractMult && extractMult.length) {
    var phoneNos = extractMult.slice(1);
    if(phoneNos.length) {
      src[self.number] = phoneNos[1];
    }
  }

  var unformattedNo = src[self.number];
  libphonenumber.e164(unformattedNo, configLoader.getConfig().app.settings.countryCode, function(err, formattedNo) {
    if(formattedNo) {
      src[self.number] = formattedNo;
    }

    if(isDuplicate(self, src[self.dupTest.srcField], dest)) {
      return callback(null, dest);
    }
    // console.log('rawNo ' + unformattedNo + ' | formattedNo ' + formattedNo);
    mergeMimeFieldArray(src, dest, self, callback);
  });
}




// Email
var emailMime = {};
emailMime.content_item_type = "vnd.android.cursor.item/email_v2";
// emailMime.content_type = "vnd.android.cursor.dir/email_v2";
emailMime.address = "data1";
emailMime.type = commonMime.type;
emailMime.display_name = "data4";
emailMime.type_home = 1;
emailMime.type_work = 2;
emailMime.type_other = 3;
emailMime.type_mobile = 4;

emailMime.schema = {
  "type": "",
  "data": ""
};

emailMime.mapping = {
  address: "data",
  type: "type"
};

emailMime.destField = 'emails';
emailMime.destFieldIsArray = true;

emailMime.dupTest = {
  srcField: emailMime.address,
  destField: emailMime.mapping.address
};




// Postal
var postalMime = {};
postalMime.content_item_type = "vnd.android.cursor.item/postal-address_v2";
// postalMime.content_type = "vnd.android.cursor.dir/postal-address_v2";
postalMime.formatted_address = "data1";
postalMime.street = "data4";
postalMime.pobox = "data5";
postalMime.neighborhood = "data6";
postalMime.city = "data7";
postalMime.region = "data8";
postalMime.postcode = "data9";
postalMime.country = "data10";
postalMime.type_home = 1;
postalMime.type_work = 2;
postalMime.type_other = 3;

postalMime.schema = {
  "city": "",
  "country": "",
  "pobox": "",
  "poCode": "",
  "region": "",
  "street": "",
  "extAddress": "",
  "fmtAddress": ""
};

postalMime.mapping = {
  street: "street",
  pobox: "pobox",
  postcode: "poCode",
  region: "region",
  city: "city",
  country: "country",
  neighborhood: "extAddress",
  formatted_address: "fmtAddress"
};

postalMime.destField = 'address';
postalMime.destFieldIsArray = true;

postalMime.dupTest = {
  srcField: postalMime.street,
  destField: postalMime.mapping.street
};





// Im
var imMime = {};
imMime.content_item_type = "vnd.android.cursor.item/im";
imMime.protocol = "data5";
imMime.custom_protocol = "data6";
imMime.type_home = 1;
imMime.type_work = 2;
imMime.type_other = 3;
imMime.protocol_custom = -1;
imMime.protocol_aim = 0;
imMime.protocol_msn = 1;
imMime.protocol_yahoo = 2;
imMime.protocol_skype = 3;
imMime.protocol_qq = 4;
imMime.protocol_google_talk = 5;
imMime.protocol_icq = 6;
imMime.protocol_jabber = 7;
imMime.protocol_netmeeting = 8;
// Not implemented for now



// Relation
var relationMime = {};
relationMime.content_item_type = "vnd.android.cursor.item/relation";
relationMime.name = "data1";
relationMime.type = commonMime.type;
relationMime.type_assistant = 1;
relationMime.type_brother = 2;
relationMime.type_child = 3;
relationMime.type_domestic_partner = 4;
relationMime.type_father = 5;
relationMime.type_friend = 6;
relationMime.type_manager = 7;
relationMime.type_mother = 8;
relationMime.type_parent = 9;
relationMime.type_partner = 10;
relationMime.type_referred_by = 11;
relationMime.type_relative = 12;
relationMime.type_sister = 13;
relationMime.type_spouse = 14;

relationMime.schema = {
  "type": "",
  "name": ""
};

relationMime.mapping = {
  name: "name",
  type: "type"
};

relationMime.destField = 'relations';
relationMime.destFieldIsArray = true;

relationMime.dupTest = {
  srcField: relationMime.name,
  destField: relationMime.mapping.name
};




// Organization
var orgMime = {};
orgMime.content_item_type = "vnd.android.cursor.item/organization";
orgMime.company = "data1";
orgMime.type = commonMime.type;
orgMime.title = "data4";
orgMime.department = "data5";
orgMime.job_description = "data6";
orgMime.symbol = "data7";
orgMime.phonetic_name = "data8";
orgMime.office_location = "data9";
orgMime.type_work = 1;
orgMime.type_other = 2;

orgMime.schema = {
  "type": "",
  "title": "",
  "job_description": "",
  "company": "",
  "location": "",
  "department": ""
};

orgMime.mapping = {
  type: "type",
  title: "title",
  department: "department",
  job_description: "job_description",
  company: "company",
  office_location: "location"
};

orgMime.destField = 'orgs';
orgMime.destFieldIsArray = true;

orgMime.dupTest = {
  srcField: orgMime.company,
  destField: orgMime.mapping.company
};


// Event
var eventMime = {};
eventMime.content_item_type = "vnd.android.cursor.item/contact_event";
eventMime.start_date = "data1";
eventMime.type = commonMime.type;
eventMime.type_anniversary = 1;
eventMime.type_other = 2;
eventMime.type_birthday = 3;

eventMime.schema = {
  "type": "",
  "date": ""
};

eventMime.mapping = {
  start_date: "date",
  type: "type"
};

eventMime.destField = 'events';
eventMime.destFieldIsArray = true;

eventMime.dupTest = {
  srcField: eventMime.start_date,
  destField: eventMime.mapping.start_date
};



// Note
var noteMime = {};
noteMime.content_item_type = "vnd.android.cursor.item/note";
noteMime.note = "data1";

noteMime.destField = 'notes';

noteMime.parse = function(src, dest, callback) {
  var self = this;
  if(!_.isArray(dest[self.destField])) {
    dest[self.destField] = [];
  }

  if(!_.contains(dest[self.destField], src[noteMime.note])) {
    dest[self.destField].push(src[noteMime.note]);
  }

  callback(null, dest);
}


// Website
var websiteMime = {};
websiteMime.content_item_type = "vnd.android.cursor.item/website";
websiteMime.url = "data1";
websiteMime.type = commonMime.type;
websiteMime.type_homepage = 1;
websiteMime.type_blog = 2;
websiteMime.type_profile = 3;
websiteMime.type_home = 4;
websiteMime.type_work = 5;
websiteMime.type_ftp = 6;
websiteMime.type_other = 7;

websiteMime.schema = {
  "type": "",
  "url": ""
};

websiteMime.mapping = {
  url: "url",
  type: "type"
};

websiteMime.destField = 'websites';
websiteMime.destFieldIsArray = true;

websiteMime.dupTest = {
  srcField: websiteMime.url,
  destField: websiteMime.mapping.url
};



// Identity
var identityMime = {};
identityMime.content_item_type = "vnd.android.cursor.item/identity";
identityMime.identity = "data1";
identityMime.namespace = "data2";
// Not implemented


// SipAddress
var sipMime = {};
sipMime.content_item_type = "vnd.android.cursor.item/sip_address";
sipMime.sip_address = "data1";
sipMime.type_home = 1;
sipMime.type_work = 2;
sipMime.type_other = 3;
// Not implemented


// Photo
var photoMime = {};
photoMime.content_item_type = "vnd.android.cursor.item/photo";
photoMime.photo_file_id = "data14";
photoMime.photo = "data15";
// Not implemented



function mergeMimeField(src, dest, mime, callback) {
  if(!_.isObject(dest[mime.destField])) {
    dest[mime.destField] = _.clone(mime.schema);
  }

  dest = mergeData(mime, src, dest, false);
  callback(null, dest);
};


function mergeMimeFieldArray(src, dest, mime, callback) {
  if(!_.isArray(dest[mime.destField])) {
    dest[mime.destField] = [];
  }

  dest = mergeData(mime, src, dest, true);
  callback(null, dest);
};



function mergeData(mime, src, dest, isArrayMerge) {

  var destObj;

  if(isArrayMerge) {
    destObj = _.clone(mime.schema);
  } else {
    destObj = dest[mime.destField];
  }

  destObj.data_id = src.__id;

  var srcKey, destKey, srcVal;
  _.each(mime.mapping, function(destKey, k) {
    srcKey = mime[k];
    srcVal = trim(src[srcKey]);
    if(!!src[srcKey] && (!destObj[destKey] || srcVal.length > destObj[destKey].length)) {
      destObj[destKey] = srcVal;
    }
  });

  if(isArrayMerge) {
    dest[mime.destField].push(destObj);
  }

  return dest;
}


function isDuplicate(mime, data, dest) {
  return _.contains(_.pluck(dest[mime.destField], mime.dupTest.destField), trim(data));
}


function matchesType(src, mime) {
  return (!!src.mimetype && src.mimetype === mime.content_item_type);
}


function trim(str) {
  if(!str) return str;
  return str.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' ');
}


const DATA_WEIGHTS = {
  PHONE: 10,
  EMAIL: 4,
  LAST_NAME: 3,
  FIRST_NAME: 2
};



var DataParser = function() {
  this.mimes = [ nameMime, nickMime, emailMime, phoneMime, relationMime, orgMime, eventMime, websiteMime, postalMime, noteMime ];
};


DataParser.prototype.addMeta = function(parsedContact) {
  if(!parsedContact) {
    return parsedContact;
  }

  if(!parsedContact.phones) {
    parsedContact.phones = [];
  }

  if(!parsedContact.emails) {
    parsedContact.emails = [];
  }

  parsedContact.meta = {
    phones: parsedContact.phones.length,
    emails: parsedContact.emails.length,
    firstName: (parsedContact.name.firstName || '').length,
    lastName: (parsedContact.name.lastName || '').length,
    displayName: (parsedContact.name.displayName || parsedContact.display_name || '').length,
  };

  parsedContact.weight = (parsedContact.meta.phones * DATA_WEIGHTS.PHONE)
                       + (parsedContact.meta.emails * DATA_WEIGHTS.EMAIL)
                       + (parsedContact.meta.firstName * DATA_WEIGHTS.FIRST_NAME)
                       + (parsedContact.meta.lastName * DATA_WEIGHTS.LAST_NAME);
  return parsedContact;
}


DataParser.prototype.parse = function(src, dest, callback) {
  var self = this;
  var parsed = false;
  var mimeType;

  for(var i = 0; i < self.mimes.length; i++) {
    mimeType = self.mimes[i];

    if(matchesType(src, mimeType)) {

      if(_.isFunction(mimeType.parse)) {
        mimeType.parse(src, dest, callback);
        parsed = true;
      } else if(!!mimeType.mapping && !!mimeType.schema && !!mimeType.destField) {
        if(mimeType.destFieldIsArray) {

          if(isDuplicate(mimeType, src[mimeType.dupTest.srcField], dest)) {
            callback(null, dest);
          } else {
            mergeMimeFieldArray(src, dest, mimeType, callback);
          }

          parsed = true;
        } else {
          mergeMimeField(src, dest, mimeType, callback);
          parsed = true;
        }
      }

      break;
    }
  }

  if(!parsed) {
    // console.log('no parser found for ' + src.mimetype + ' | ' + mimeType.content_item_type)
    callback(new Error('no parser found for ' + src.mimetype));
  }
}


DataParser.prototype.mergeSync = function(contact1, contact2) {

  var srcContact = contact1, destContact = contact2;
  var self = this;

  // if(contact2.weight >= contact1.weight) {
  //   destContact = contact2;
  //   srcContact = contact1;
  // } else {
  //   destContact = contact1;
  //   srcContact = contact2;
  // }

  destContact = self.mergeName(srcContact, destContact);

  _.each(self.mimes, function(mime) {
    if(mime.destFieldIsArray) {
      destContact = self.mergeContactArrayField(mime, srcContact, destContact);
    }
  });

  delete destContact._id;
  return destContact;
}


DataParser.prototype.mergeName = function(srcContact, destContact) {

  var self = this;

  var nameTmpl = _.clone(nameMime.schema);
  if(!_.isObject(destContact.name)) {
    destContact.name = nameTmpl;
  }

  if(!srcContact.name) {
    return destContact;
  }

  var merged = false;

  // If lastName and firstName exist, it is used assuming they provide more clarity
  if((srcContact.meta.lastName > destContact.meta.lastName && srcContact.meta.firstName > 0) || (srcContact.meta.displayName > destContact.meta.displayName)) {
    destContact.name = _.defaults(srcContact.name, nameTmpl);
    destContact.name.displayName = destContact.display_name = srcContact.display_name || srcContact.name.displayName || '';
    merged = true;
  }

  if(merged) {
    destContact.name.nick = srcContact.name.nick;
  }

  return self.addMeta(destContact);
}


DataParser.prototype.mergeContactArrayField = function(mime, srcContact, destContact) {
  var self = this;

  var field = mime.destField;
  destContact[field] = destContact[field] || [];

  _.each(srcContact[field] || [], function(obj) {

    if(!isDuplicate(mime, obj[mime.dupTest.destField], destContact)) {
      destContact[field].push(obj);
    }

  });

  return self.addMeta(destContact);
}



DataParser.prototype.mergeNotes = function(srcContact, destContact) {
  var self = this;


  return self.addMeta(destContact);
}



module.exports.DataParser = new DataParser();