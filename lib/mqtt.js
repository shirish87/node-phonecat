
var mqtt = require('mqtt')
  , configLoader = require('../configLoader')
  , TopicRouter = require('./mqttTopicRouter').TopicRouter
  , PhonebookController = require('../controller/phonebook').PhonebookController



module.exports = function(app) {

  config = configLoader.getConfig();
  var port = config.app.mqtt.port;

  var topicRouter = new TopicRouter([ PhonebookController ]);


  mqtt.createServer(function(client) {

    var self = this;

    if (!self.clients) self.clients = {};

    client.on('connect', function(packet) {
      client.connack({ returnCode: 0 });
      client.id = packet.clientId;
      client.subscriptions = [];
      self.clients[client.id] = client;
    });


    client.on('publish', function(packet) {
      topicRouter.route(client, packet, function(err, data) {
        if(err) {
          return console.log('route error', err);
        }

        if(data) {
          client.publish({ topic: packet.topic, payload: data });
        }
      });
    });


    client.on('subscribe', function(packet) {
      var granted = [];

      for (var i = 0; i < packet.subscriptions.length; i++) {
        var qos = packet.subscriptions[i].qos
          , topic = packet.subscriptions[i].topic
          , reg = new RegExp(topic.replace('+', '[^\/]+').replace('#', '.+') + '$');

        granted.push(qos);
        client.subscriptions.push(reg);
      }

      client.suback({messageId: packet.messageId, granted: granted});
    });

    client.on('pingreq', function(packet) {
      client.pingresp();
    });

    client.on('disconnect', function(packet) {
      // console.log('disconnect', packet);
      client.stream.end();
    });

    client.on('close', function(err) {
      // console.log('close', err);
      delete self.clients[client.id];
    });

    client.on('error', function(err) {
      // console.log('error', err);
      client.stream.end();
    });

  }).listen(port, function() {
    console.log('MQTT listening on ' + port);
  });

}
