

var namespaceSeparator = module.exports.namespaceSeparator = ':';


var TopicRouter = module.exports.TopicRouter = function(controllers){
  this.topicRoutes = {};
  this.controllers = controllers;
  this.init();
};


TopicRouter.prototype.init = function() {
  var self = this;

  this.controllers.forEach(function(controller) {

    if(!controller.namespace || !controller.publicMethods){
      console.log('missing namespace or publicMethods');
      return;
    }

    controller.publicMethods.forEach(function(method){

      var topic = controller.namespace + namespaceSeparator + method;

      self.topicRoutes[topic] = {
        action: controller[method].bind(controller)
      };

      if(typeof controller.transformData === 'function') {
        self.topicRoutes[topic].transformData = controller.transformData.bind(controller);
      }

    });

  });
}


TopicRouter.prototype.route = function(client, data, callback) {

  var topic = data.topic;
  var topicRoute = this.topicRoutes[topic];

  if(!topicRoute) {
    // route not found
    return callback(new Error('Route not found: ' + topic));
  }

  if(!data || !data.payload) {
    return callback(new Error('Data or payload not found'));
  }

  // process data
  var payload = data.payload;

  if(topicRoute.transformData) {
    payload = topicRoute.transformData.call(topicRoute, payload);
  }

  topicRoute.action.apply(topicRoute, [ client, payload, callback ]);
}
