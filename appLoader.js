var EventEmitter = require('events').EventEmitter
  , util = require('util')
  , path = require('path')
  , Config = require('./configLoader')
  , express = require('express')
  , flash = require('connect-flash')


var AppLoader = function(){

};

util.inherits(AppLoader, EventEmitter);


AppLoader.prototype.init = function(app){
  this.app = app;
  var self = this;

  this.emit('preConfigHook', app);
  this.loadConfig();
  this.configExceptions(function(err) {
    self.emit('exceptionHook', err, app);
  });
  this.emit('postConfigHook', app);


  this.emit('preAppHook', app);
  this.configApp();
  this.emit('postAppHook', app);

  this.emit('preSessionHook', app);
  this.configSession(function(sessionConfig) {
    self.emit('sessionHook', app, sessionConfig);
  });
  this.emit('postSessionHook', app);

  this.emit('preStaticHook', app);
  this.configStatic();
  this.emit('postStaticHook', app);

  this.emit('preRouterHook', app);
  this.configRouter(function() {
    self.emit('routerHook', app);
  });
  this.emit('postRouterHook', app);

  this.emit('done', app);
  
  return this;
}



AppLoader.prototype.loadConfig = function(){
  Config.init();
  this.config = Config.getConfig();
  this.env = this.config.env;
}



AppLoader.prototype.configApp = function(){
  var app = this.app;
  
  app.set('apiPath', this.config.app.api.path);
  app.set('views', this.config.app.env.rootDir + '/views');
  app.set('view engine', 'jade');

  app.use(express.favicon());
  app.use(express.bodyParser());
}



AppLoader.prototype.configSession = function(fnSessionHook){
  var app = this.app;

  // session support
  app.use(express.cookieParser());
  
  var sessionConfig = {
    key: '_sess',
    secret: 'dasds21dkds22as2jsjsad%'
  };

  if(typeof fnSessionHook === 'function') {
    fnSessionHook(sessionConfig);
  }

  app.use(express.session(sessionConfig));
  app.use(flash());
}


AppLoader.prototype.configStatic = function(){
  var app = this.app;

  // Note: static/public files here are free from routing
  var publicFolder = path.join(this.config.app.env.rootDir, 'public');
  app.use(express.static(publicFolder));
}


AppLoader.prototype.configRouter = function(fnRouterHook){
  var app = this.app;

  app.use(express.methodOverride());

  if(typeof fnRouterHook === 'function') {
    fnRouterHook();
  }
  app.use(app.router);
}


AppLoader.prototype.configExceptions = function(fnException){
  process.on('uncaughtException', function (err) {
    if(typeof fnException === 'function') {
      fnException(err);
    }
  });
}


module.exports = new AppLoader();
