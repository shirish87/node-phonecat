
var env = process.env.NODE_ENV = (typeof process.env.NODE_ENV !== 'undefined') ? process.env.NODE_ENV : 'test';
if(env !== 'test') throw new Error('Invalid env');

var mqtt = require('mqtt')
  , configLoader = require('../configLoader').init(env, false)
  , fork = require("child_process").fork


var host = '127.0.0.1';

var state = {};

describe("Contacts Service", function() {
  this.timeout(15000);

  var child;

  before(function (done) {
    child = fork('app.js', null, { env: { PORT: 5555, MQTT_PORT: 5556 } });
    child.on('message', function (msg) {
      if (msg === 'started') {
        done();
      }
    });
  });

  after(function () {
    child.kill();
  });


  it('connect client', function(done) {

    var config = configLoader.getConfig();
    state.client = mqtt.createClient(config.app.mqtt.port, host);
    state.client.once('error', function(err) {
      done(err);
    });

    state.client.once('connect', function() {
      done();  
    });

  });


  it('Publish message', function(done) {
    var client = state.client;

    var topic = 'phonebook:startUpload';

    client.on('message', function(topic, message) {
      console.log('topic: %s, message: %s', topic, message);
      client.removeAllListeners();
      done();
    });

    client.subscribe(topic);

    // string
    client.publish(topic, 'hello');

  });



  it('Publish new contact', function(done) {
    var client = state.client;

    var topic = 'phonebook:addContact';

    client.on('message', function(topic, message) {
      console.log('topic: %s, message: %s', topic, message);
      done();
    });

    client.subscribe(topic);

    // string
    client.publish(topic, JSON.stringify({
      _id: {
        type: "int",
        data: 1
      },
      name: {
        type: "string",
        data: "Shirish Kamath"
      }
    }));

  });
});