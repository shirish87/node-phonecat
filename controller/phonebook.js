var _ = require('lodash')
  , Config = require('../configLoader')
  , async = require('async')
  , Datastore = require('nedb')
  , util = require('util')
  , Strutils = require('../lib/strutils')
  , DataParser = require('../lib/mimetypes').DataParser
  , fs = require('fs')
  , sqlite3 = require('sqlite3').verbose()
  , jf = require('jsonfile')
  , debug = require('debug')('phonebook')
  , debugDedup = require('debug')('dedup')
  , multimeter = require('multimeter')
  , multi = multimeter(process)


const PARALLEL_TASKS = 4;

const PATH_DB_CONTACTS = 'db/contacts_contacts.json';
const PATH_DB_RAWCONTACTS = 'db/contacts_rawcontacts.json';
const PATH_DB_DATA = 'db/contacts_data.json';
const PATH_DB_GROUPS = 'db/contacts_groups.json';

const PATH_DB_FB = 'db/contacts_fb.json';
const PATH_DB_RESULT = 'db/contacts_tmp.json';
const PATH_DB_DEDUP = 'db/contacts_dedup.json';


const PATH_INPUT = 'input/';
const PATH_INPUT_CONTACTS = PATH_INPUT + 'contacts.json';
const PATH_INPUT_RAWCONTACTS = PATH_INPUT + 'raw_contacts.json';
const PATH_INPUT_DATA = PATH_INPUT + 'data.json';
const PATH_INPUT_GROUPS = PATH_INPUT + 'groups.json';

const PATH_OUTPUT = 'output/';
const PATH_OUTPUT_RESULT = PATH_OUTPUT + 'contacts.json';

jf.spaces = 2;


// var dbContacts = new Datastore({ filename: PATH_DB_CONTACTS, autoload: true });
// var dbRawContacts = new Datastore({ filename: PATH_DB_RAWCONTACTS, autoload: true });
// var dbData = new Datastore({ filename: PATH_DB_DATA, autoload: true });
// var dbGroups = new Datastore({ filename: PATH_DB_GROUPS, autoload: true });

// [ dbContacts, dbRawContacts, dbData, dbGroups ].forEach(function(db){
//   db.ensureIndex({ fieldName: '__id', unique: true }, function(err) {
//     if(err) {
//       debug('ensureIndex err', err);
//     }
//   });
// });



var print = function(name, obj){
  if(arguments.length == 1){
    obj = name;
    name = '';
  }
  debug(name, util.inspect(obj, { depth: null }));
}



var PhonebookController = function() {
  this.namespace = 'phonebook';
  // this.publicMethods = [ 'startUpload', 'endUpload', 'addContact', 'addRawContact', 'addData' ];
  this.publicMethods = _.methods(this);
}


PhonebookController.prototype.startUpload = function(client, data, callback) {
  debug('startUpload');
  return callback(null, 'success');
}

PhonebookController.prototype.endUpload = function(client, data, callback) {

}


PhonebookController.prototype.addContact = function(client, data, callback) {
  debug(JSON.stringify(data));
  dbContacts.insert(data, function(err) {
    if(err) {
      return callback(null, err.message || 'error');
    }

    return callback(null, 'success');
  });
}

PhonebookController.prototype.addRawContact = function(client, data, callback) {
  debug(JSON.stringify(data));
  dbRawContacts.insert(data, function(err) {
    if(err) {
      return callback(null, err.message || 'error');
    }

    callback(null, 'success');
  });
}

PhonebookController.prototype.addData = function(client, data, callback) {
  debug(JSON.stringify(data));
  dbData.insert(data, function(err) {
    if(err) {
      return callback(null, err.message || 'error');
    }

    return callback(null, 'success');
  });
}




PhonebookController.prototype.checkInputs = function(done) {
  var self = this;
  fileExists(PATH_INPUT_CONTACTS, function(err) {
    if(err) { return done(err);  }

    fileExists(PATH_INPUT_RAWCONTACTS, function(err) {
      if(err) { return done(err);  }

      fileExists(PATH_INPUT_DATA, done);
    });

  });
}


PhonebookController.prototype.convertInput = function(inputPath, nedb, done) {
  jf.readFile(inputPath, function(err, rows) {
    if(err) { return done(err); }

    var currIndex = 0;
    var total = rows.length;

    multi.drop(function(pbar) {
      async.eachLimit(rows, PARALLEL_TASKS, function(row, cb) {
        pbar.ratio(++currIndex, total);
        nedb.insert(nedbSafe(row), cb);
      }, done);
    });

  });
}


PhonebookController.prototype.getLoadAction = function(inputPath, dbPath, inputsExist) {
  var self = this;

  return function(done) {
    initDb(dbPath, function(err, nedb) {
      if(err) { return done(err); }

      if(!inputsExist) {
        return done(null, nedb);
      }

      debug('Loading input data from %s', inputPath);
      self.convertInput(inputPath, nedb, function(err) {
        if(err) { return done(err); }
        return done(null, nedb);
      });
    });
  };
}



PhonebookController.prototype.loadInputs = function(callback) {
  var self = this;

  async.waterfall([
    function(done) {
      self.checkInputs(function(err) {
        return done(null, !err);
      });
    },
    function(inputsExist, done) {

      async.parallel({
        dbContacts: self.getLoadAction(PATH_INPUT_CONTACTS, PATH_DB_CONTACTS, inputsExist),
        // dbRawContacts, self.getLoadAction(PATH_INPUT_RAWCONTACTS, PATH_DB_RAWCONTACTS, inputsExist),
        dbData: self.getLoadAction(PATH_INPUT_DATA, PATH_DB_DATA, inputsExist)
      }, function(err, result) {
        debug('nedb databases initialized');
        return done(err, result);
      });

    }
  ], callback);
}


PhonebookController.prototype.buildContact = function(contact, dataRows, callback) {
  // build contact from all data rows associated with this contact
  var parsedContact = {};
  parsedContact.contact_id = contact.__id;
  parsedContact.display_name = contact.display_name;
  parsedContact.name_hash = '';

  async.eachSeries(dataRows, function(data, done) {
    setImmediate(function() {
      // parse each data row belonging to current contact
      DataParser.parse(data, parsedContact, function(ignoreErr, result) {
        if(result) {
          parsedContact = result;
        }
        done();
      });
    });
  }, function(err) {
    if(err) { return callback(err); }

    if(!parsedContact.name) {
      console.log(JSON.stringify(parsedContact));
    }

    // one full contact built
    parsedContact = DataParser.addMeta(parsedContact);
    if(parsedContact.meta.lastName > 0) {
      parsedContact.name_hash = ((parsedContact.name.firstName || '') + parsedContact.name.lastName).toLowerCase();
    }
    return callback(null, parsedContact);
  });
}


PhonebookController.prototype.sanitize = function(client, data, callback) {

  var self = this;
  var dbContacts, dbData;


  async.waterfall([
    function(done) {
      debug('Loading input files');
      self.loadInputs(function(err, result) {
        if(!result.dbContacts || !result.dbData) {
          return done(new Error('Error loading nedb databases: ' + err));
        }

        dbContacts = result.dbContacts;
        dbData = result.dbData;
        return done();
      });
    },
    function(done) {
      debug('Reading all phone contacts');
      dbContacts.find({}, done);
    },
    function(contacts, done) {
      debug('Reading data for all phone contacts  ');
      multi.drop(function(pbar) {
        return done(null, contacts, pbar);
      });
    },
    function(contacts, pbar, done) {

      var parsedContactList = [];
      var currContact = 0;
      var contactsLen = contacts.length;

      async.eachLimit(contacts, PARALLEL_TASKS, function(contact, cb) {

        pbar.ratio(++currContact, contactsLen);

        dbData.find({ contact_id: contact.__id }, function(err, dataRows) {
          if(err) { return cb(err); }

          setImmediate(function() {
            self.buildContact(contact, dataRows, function(err, parsedContact) {
              if(err) { return cb(err); }

              // for "e-mail only" contacts
              if(!parsedContact.name.displayName || parsedContact.name.displayName.length === 0 || parsedContact.name.displayName === 'null') {
                parsedContact.name.displayName = parsedContact.display_name;
              }

              parsedContactList.push(parsedContact);
              return cb();
            });
          });

        });

      }, function(err) {
        debug('Done');
        return done(err, parsedContactList);
      });

    },
    function(parsedContactList, done) {
      debug('Saving parsed contacts');
      self.saveContactList(parsedContactList, done);
    },
    function(contactList, dbResult, done) {
      debug('Deduplicating data');
      self.dedup(dbResult, done);
    },
    function(dbResult, done) {
      initDb(PATH_DB_DEDUP, function(err, dbDedup) {
        return done(err, dbResult, dbDedup);
      });
    },
    function(dbResult, dbDedup, done) {
      copyDb(dbResult, dbDedup, done);
    },
    function(contactList, done) {

      var output = {
        meta: {
          count: contactList.length
        },
        contacts: contactList
      };

      jf.writeFile(PATH_OUTPUT_RESULT, output, done);
    }
  ], function(err, result) {
    debug('Done');
    return callback(err, result);
  });

  // fbController.mergeWithPhoneContact(dbFacebook, parsedContact, function(err, contact) {
  //   if(err) {
  //     // debug(err);
  //     parsedContactList.push(parsedContact);
  //   } else {
  //     parsedContactList.push(contact);
  //   }
  //   cb();
  // });
}



PhonebookController.prototype.dedup = function(dbResult, callback) {

  // superbly memory-intensive!
  var dedupBuckets = {
    phones: {},
    emails: {},
    name_hash: {}
  };

  async.series([
    function(callback) {
      debugDedup('Deduping by phone');

      dbResult.find({}, function(err, contacts) {
        if(err) { return callback(err); }
        debugDedup('Contacts count: %d', contacts.length);

        async.eachSeries(contacts, function(contact, cb) {

          var fnDedupByPhone = dedupArray(dedupBuckets, 'phones', 'number', contact, dbResult);
          fnDedupByPhone(cb);

        }, function(err) {
          return callback(err);
        });
      });

    },

    function(callback) {
      debugDedup('Deduping by email');

      dbResult.find({}, function(err, contacts) {
        if(err) { return callback(err); }
        debugDedup('Contacts count: %d', contacts.length);

        async.eachSeries(contacts, function(contact, cb) {

          var fnDedupByEmail = dedupArray(dedupBuckets, 'emails', 'data', contact, dbResult);
          fnDedupByEmail(cb);

        }, function(err) {
          return callback(err);
        });
      });
    },

    function(callback) {
      debugDedup('Deduping by name');

      dbResult.find({}, function(err, contacts) {
        if(err) { return callback(err); }
        debugDedup('Contacts count: %d', contacts.length);

        async.eachSeries(contacts, function(contact, cb) {

          if(contact.name_hash.length == 0) {
            return cb();
          }

          var fnDedupByNameHash = dedupScalar(dedupBuckets, 'name_hash', contact, dbResult)
          fnDedupByNameHash(cb);

        }, function(err) {
          return callback(err);
        });
      });
    }
  ], function(err) {
    return callback(err, dbResult);
  });
}


PhonebookController.prototype.saveContactList = function(contactList, callback) {

  debug('Saving contact list');
  if(!contactList || !contactList.length) {
    return callback(null, contactList);
  }

  initDb(PATH_DB_RESULT, function(err, dbResult) {

    debug('Store nedb contactlist');
    async.eachLimit(contactList, PARALLEL_TASKS, function(row, cb) {

      dbResult.insert(row, function(err) {
        if(err) { return cb(err); }
        cb();
      });

    }, function(err) {
      debug('Done');
      return callback(err, contactList, dbResult);
    });
  });
}


// method to pre-process data (packet.payload)
PhonebookController.prototype.transformData = function(data) {
  try {
    data = nedbSafe(JSON.parse(data));
  } catch(e) {}

  return data;
}



var FacebookController = function() {

};

FacebookController.prototype.load = function(inputPath, callback) {
  debug('Loading Facebook contacts into nedb');
  var self = this;
  self.convertSqliteInput(inputPath, "SELECT data FROM contacts", PATH_DB_FB, callback);
}


FacebookController.prototype.convertSqliteInput = function(inputPath, query, dbPath, done) {
  var self = this;

  async.waterfall([
    function(callback) {
      fileExists(inputPath, callback);
    },
    function(fstat, callback) {
      debug('Init Facebook contacts db');
      initDb(dbPath, callback);
    },
    function(nedb, callback) {
      var sourceDb = new sqlite3.Database(inputPath);
      sourceDb.serialize(function() {
        debug('Reading Facebook contacts from sqlite');
        sourceDb.all(query, function(err, rows) {
          return callback(err, rows, nedb);
        });
      });
    },
    function(rows, nedb, callback) {
      self.parse(rows, nedb, callback);
    }
  ], done);
}


FacebookController.prototype.mergeWithPhoneContact = function(dbFacebook, phoneContact, callback) {
  var phones = _.pluck(phoneContact.phones, 'number') || [];
  if(!phones.length) {
    return callback(null, phoneContact);
  }

  var query = { lookupPhones: { $in: phones } };

  if(phoneContact.name && phoneContact.name.displayName) {
      query = { $or: [ query, { lookupNames: phoneContact.name.displayName.toLowerCase() } ] };
  }

  dbFacebook.findOne(query, function(err, doc) {
    if(err) { debug(err); }
    if(doc) {
      var fbPhones = _.pluck(doc.phones, 'universalNumber') || [];
      debug('fb_contact: ' + doc.name.displayName);
      // merge logic here
    }
    return callback(null, phoneContact);
  });
}


FacebookController.prototype.parse = function(rows, dbFacebook, callback) {
  debug('Parsing Facebook contacts and storing in nedb');

  var currIndex = 0;
  var total = rows.length;

  multi.drop(function(pbar) {

    async.eachLimit(rows, PARALLEL_TASKS, function(row, cb) {

      pbar.ratio(++currIndex, total);

      var newData = _.pick(JSON.parse(row.data), [ 'name', 'phones', 'friendshipStatus', 'canMessage', 'contactType', 'subscribeStatus', 'isMobilePushable' ]);
      newData.lookupPhones = _.pluck(newData.phones, 'universalNumber') || [];
      newData.lookupNames = [ newData.name.displayName.toLowerCase() ];
      // _.map(newData.name, function(val, key) {
      //   if(!!val) {
      //     newData.lookupNames.push(val.toLowerCase());
      //   }
      // });

      dbFacebook.insert(newData, cb);

    }, function(err) {
      debug('Done');
      return callback(err, dbFacebook);
    });

  });

}


function hash(s) {
  var hash = 0,
      strlen = s.length,
      i,
      c;

  if(strlen === 0) {
    return hash;
  }

 for(i = 0; i < strlen; i++) {
  c = s.charCodeAt(i);
  hash = ((hash << 5) - hash) + c;
  hash = hash & hash; // Convert to 32bit integer
 }
 return hash;
}


function mergeAndRemove(contact, dbResult, dedupBuckets, field, val, cb) {
  var lookupHash = dedupBuckets[field][val];

  // retrieve stored contacts
  var query = { _id: { $in: [ lookupHash._id, contact._id ] } };

  dbResult.find(query, function(err, results) {

    if(err || !results || results.length != 2) {
      debugDedup(err);
      return cb(err);
    }

    // merge with current contact
    dbResult.insert(DataParser.mergeSync(results[0], results[1]), function(err, newContact) {
      if(err) {
        debugDedup(err);
        return cb(err);
      }

      // update hash
      dedupBuckets[field][val] = {
        _id: newContact._id,
        display_name: newContact.display_name
      };

      // remove source contacts
      dbResult.remove(query, { multi: true }, function(err) {
        return cb(err);
      });

    });

  });
}




function dedupScalar(dedupBuckets, field, contact, dbResult) {

  return function(cb) {
    // debugDedup('dedupScalar %s', field);

    var val = (contact[field] || '').toLowerCase();
    var lookupHash = dedupBuckets[field][val];

    if(!!lookupHash) {

      debugDedup('Found dup scalar (%s): %s (%s) | %s (%s)',
        field,
        contact.display_name,
        contact._id,
        lookupHash.display_name,
        lookupHash._id
      );

      mergeAndRemove(contact, dbResult, dedupBuckets, field, val, cb);
    } else {
      dedupBuckets[field][val] = {
        _id: contact._id,
        display_name: contact.display_name
      };

      return cb();
    }

  }
}


function dedupArray(dedupBuckets, field, dataField, contact, dbResult) {

  return function(callback) {
    // debugDedup('dedupArray %s.%s', field, dataField);

    async.eachSeries(contact[field], function(obj, cb) {

      setImmediate(function(){

        var val = (obj[dataField] || '').toLowerCase();

        var lookupHash = dedupBuckets[field][val];

        if(!!lookupHash) {
          debugDedup('Found dup: %s (%s) | %s (%s)',
            contact.display_name,
            contact._id,
            lookupHash.display_name,
            lookupHash._id
          );

          mergeAndRemove(contact, dbResult, dedupBuckets, field, val, cb);

        } else {

          dedupBuckets[field][val] = {
            _id: contact._id,
            display_name: contact.display_name
          };

          cb();
        }

      });

    }, function(err) {
      return callback(err, contact);
    });

  };
}


function copyDb(dbSrc, dbDest, callback) {
  dbSrc.find({}, function(err, docs) {
    if(err) { return callback(err); }
    debugDedup('docs count: %d', docs.length);

    async.eachLimit(docs, PARALLEL_TASKS, function(doc, cb) {
      delete doc._id;

      dbDest.insert(doc, function(err) {
        return cb(err);
      });

    }, function(err){
      return callback(err, docs);
    })

  });
}



function fileExists(path, callback) {
  fs.lstat(path, function(err, stats) {
    if(err) {
      return callback(err);
    }

    if (stats.isFile()) {
      return callback(null, stats);
    } else {
      return callback(new Error('Invalid file'));
    }
  });
}


function nedbSafe(data) {
  if(data._id) {
    data.__id = data._id;
    delete data._id;
  }
  return data;
}



function initDb(dbPath, callback) {
  async.waterfall([
    function(done) {
      fs.unlink(dbPath, function(ignoreErr) {
        return done();
      });
    },
    function(done) {
      debug('Creating nedb at %s', dbPath);
      var newDb = new Datastore({ filename: dbPath, autoload: true });
      return done(null, newDb);
    }
  ], callback);
}


module.exports.PhonebookController = pbController = new PhonebookController();
module.exports.FacebookController = fbController = new FacebookController();
module.exports.fileExists = fileExists;
